//index.js
//获取应用实例
const {CHANGENUM} =  require('../../store/actions.js')
var store = getApp().store

Page({
  data: {
    a:0,
    changeNum:0
  },
  onLoad () {
    this.setData(store.getState().default)
    this.setStoreData();
  },
  onShow(){
    
  },
  onUnLoad(){
    console.log('un')
    store.dispatch({ type: CHANGENUM , value:0});
    this.unsubscribe();
  },
  //事件处理函数
  actionHandle: function() {
    store.dispatch({ type: CHANGENUM , value:++store.getState().default.num});
    this.setData({changeNum:store.getState().default.num})
  },
  setStoreData(){
    this.unsubscribe = store.subscribe(() =>
      this.setData(store.getState().default)
    );
  }
  // hebing(){
  //   Object.assign(this.data,store.getState().default)
  // }
  // onLoad: function () {
  //   if (app.globalData.userInfo) {
  //     this.setData({
  //       userInfo: app.globalData.userInfo,
  //       hasUserInfo: true
  //     })
  //   } else if (this.data.canIUse){
  //     // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
  //     // 所以此处加入 callback 以防止这种情况
  //     app.userInfoReadyCallback = res => {
  //       this.setData({
  //         userInfo: res.userInfo,
  //         hasUserInfo: true
  //       })
  //     }
  //   } else {
  //     // 在没有 open-type=getUserInfo 版本的兼容处理
  //     wx.getUserInfo({
  //       success: res => {
  //         app.globalData.userInfo = res.userInfo
  //         this.setData({
  //           userInfo: res.userInfo,
  //           hasUserInfo: true
  //         })
  //       }
  //     })
  //   }
  // },
  // getUserInfo: function(e) {
  //   console.log(e)
  //   app.globalData.userInfo = e.detail.userInfo
  //   this.setData({
  //     userInfo: e.detail.userInfo,
  //     hasUserInfo: true
  //   })
  // }
})
