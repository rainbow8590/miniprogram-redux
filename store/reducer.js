	const {CHANGENUM} =  require('./actions.js')
	var data = {
		num: wx.getStorageSync('num')?wx.getStorageSync('num'): 12
	}
	
	export default (state = data, action) => {
		if(action.type == CHANGENUM){
			wx.setStorageSync('num',action.value)
			return {
				...state,
				num: action.value
			}
		}
		
		return state;
	}
